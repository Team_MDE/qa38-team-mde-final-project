package Utils;

public class JSONRequests {

    public static final String SAVE_POST_BODY = "{\n" +
            "  \"content\": \"This is my newly created post\",\n" +
            "  \"picture\": \"string\",\n" +
            "  \"public\": true\n" +
            "}";

    public static final String EDIT_POST_BODY = "{\n" +
            "    \"content\": \" \",\n" +
            "    \"picture\": \"string\",\n" +
            "    \"public\": true\n" +
            "}";
}
