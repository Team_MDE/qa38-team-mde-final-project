package Utils;

import static Utils.Constants.*;
import static java.lang.String.format;

public class Endpoints {


    public static final String LOGIN_USER_ENDPOINT = "/authenticate?username="+USERNAME+"&password="+PASSWORD;
    public static final String FIND_ALL_POSTS_ENDPOINT = "/api/post/";
    public static final String SAVE_POST_ENDPOINT = "/api/post/auth/creator?username="+USERNAME;
    public static final String SHOW_PROFILE_POSTS_ENDPOINT = format("%s%s%s","/api/users/",USERID,"/posts");
    public static final String EDIT_POSTS_ENDPOINT = "/api/post/auth/editor/?username="+USERNAME+"&postId="+POST_ID;
    public static final String LIKE_POST_ENDPOINT = "/api/post/auth/likesUp?username="+USERNAME+"&postId="+POST_ID;
    public static final String DELETE_POST_ENDPOINT = "/api/post/auth/manager";
}
