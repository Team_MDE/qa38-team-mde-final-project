package com.heroku;

import io.restassured.RestAssured;
import io.restassured.filter.cookie.CookieFilter;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;


import static Utils.Constants.*;
import static Utils.Endpoints.LOGIN_USER_ENDPOINT;
import static java.lang.String.format;

public class BaseTestSetup {
    CookieFilter cookieFilter = new CookieFilter();

    @BeforeClass
    public void authenticateUser() {

        String URI = format("%s%s", BASE_URL, LOGIN_USER_ENDPOINT);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", USERNAME);
        jsonObject.put("password", PASSWORD);

        Response response = RestAssured.given()
                .filter(cookieFilter)
                .body(jsonObject)
                .post(URI)
                .andReturn();
    }


}
