package com.heroku;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static Utils.Constants.BASE_URL;
import static Utils.Endpoints.EDIT_POSTS_ENDPOINT;
import static Utils.JSONRequests.EDIT_POST_BODY;
import static java.lang.String.format;
import static Utils.Helper.isValid;
import static org.testng.Assert.*;

public class EditPostTest extends BaseTestSetup {

    @Test
    public void editPost(){
        String URI = format("%s%s", BASE_URL, EDIT_POSTS_ENDPOINT);

        String requestBody = (format(EDIT_POST_BODY));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = RestAssured.given()
                .filter(cookieFilter)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .put(URI);
        int statusCode = response.getStatusCode();
        System.out.println("Status code is: "+ statusCode);
        System.out.println(response.getCookies());assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        System.out.println("Response body is empty" + response.getBody().prettyPrint());
        assertEquals(response.body().asString(), "", "Response body is not empty");


    }
}
