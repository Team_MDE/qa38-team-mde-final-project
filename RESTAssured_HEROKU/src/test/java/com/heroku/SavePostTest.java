package com.heroku;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static Utils.Constants.*;
import static Utils.Endpoints.SAVE_POST_ENDPOINT;
import static Utils.Helper.isValid;
import static Utils.JSONRequests.SAVE_POST_BODY;
import static java.lang.String.format;
import static org.testng.Assert.assertTrue;

public class SavePostTest extends BaseTestSetup {

    public static int postId;


    @Test
    public void savePost() {

        String URI = format("%s%s", BASE_URL, SAVE_POST_ENDPOINT);

        String requestBody = (format(SAVE_POST_BODY));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = RestAssured.given()
                .filter(cookieFilter)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post(URI);

        int statusCode = response.getStatusCode();
        System.out.println(statusCode);
        Assert.assertEquals(statusCode,200,"Incorrect status code. Expected 200.");

        System.out.println(response.getBody().jsonPath().prettify());
        Assert.assertTrue((response.getBody().jsonPath().get("content")).equals("This is my newly created post"));


        postId = response.then().extract().path("postId");
        System.out.println(postId);



    }



}
