package com.heroku;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;


import static Utils.Constants.BASE_URL;
import static Utils.Endpoints.LIKE_POST_ENDPOINT;
import static java.lang.String.format;

public class LikePostTest extends BaseTestSetup {

    @Test
    public void likePost(){
        String URI = format("%s%s", BASE_URL, LIKE_POST_ENDPOINT);


        Response response = RestAssured.given()
                .filter(cookieFilter)
                .contentType("application/json")
                .when()
                .post(URI);

        int statusCode = response.getStatusCode();
        System.out.println(statusCode);
        Assert.assertEquals(statusCode,200,"Incorrect status code. Expected 200.");

        System.out.println(response.getBody().prettyPrint());


    }

}
