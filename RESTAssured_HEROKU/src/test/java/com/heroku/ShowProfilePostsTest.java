package com.heroku;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static Utils.Constants.BASE_URL;
import static Utils.Endpoints.SHOW_PROFILE_POSTS_ENDPOINT;
import static java.lang.String.format;

public class ShowProfilePostsTest extends BaseTestSetup {

    @Test
    public void showProfilePosts(){
        String URI = format("%s%s",BASE_URL,SHOW_PROFILE_POSTS_ENDPOINT);

        Response response = RestAssured.given()
                .filter(cookieFilter)
                .contentType("application/json")
                .body("{\n" +
                        "  \"index\": 0,\n" +
                        "  \"next\": true,\n" +
                        "  \"searchParam1\": \"string\",\n" +
                        "  \"searchParam2\": \"string\",\n" +
                        "  \"size\": 10\n" +
                        "}")
                .when()
                .get(URI);

        System.out.println(response.getStatusCode());
        System.out.println(response.getBody().jsonPath().prettify());
    }
}
