package com.heroku;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static Utils.Constants.*;
import static Utils.Endpoints.*;
import static Utils.Helper.isValid;
import static java.lang.String.format;
import static org.testng.Assert.assertTrue;


public class LoginTest extends BaseTestSetup {


    @Test
    public void login() {

        String URI = format("%s%s", BASE_URL, LOGIN_USER_ENDPOINT);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", USERNAME);
        jsonObject.put("password", PASSWORD);

        Response response = RestAssured.given().filter(cookieFilter).body(jsonObject).post(URI);

        int statusCode = response.getStatusCode();
        System.out.println(statusCode);

        Assert.assertEquals(statusCode, 302, "Incorrect status code. Expected 302.");

        String cookieValue = response.getCookies().get("JSESSIONID");
        System.out.println(cookieValue);

        isValid(cookieValue);

    }

}



