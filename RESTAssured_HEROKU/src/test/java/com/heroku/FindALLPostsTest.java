package com.heroku;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static Utils.Constants.BASE_URL;
import static Utils.Endpoints.FIND_ALL_POSTS_ENDPOINT;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class FindALLPostsTest extends BaseTestSetup {

    @Test
    public void findAllPosts(){

        String URI = format("%s%s",BASE_URL,FIND_ALL_POSTS_ENDPOINT);


        Response response = RestAssured.given()
                .filter(cookieFilter)
                .queryParam("sorted","true")
                .queryParam("unsorted", "true")
                .contentType("application/json")
                .when()
                .get(URI);
        int statusCode = response.getStatusCode();

        System.out.println(response.getStatusCode());
        System.out.println(response.getBody().jsonPath().prettify());
        System.out.println(response.getCookies());
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

    }



}
