package pages.telerik.wearesocial.magdalena;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class PostPage extends BaseWebsitePage{
    public PostPage(WebDriver driver) {super(driver, "home.page");
    }

    public void createPost(String postContent) {


        navigateToPage();

        actions.waitForElementClickable("//ul/li[8]");
        actions.clickElement("//ul/li[8]");

        //https://weare-social-network.herokuapp.com/posts/auth/newPost
        actions.waitForElementClickable("//form");

        Select se = new Select(driver.findElement(By.xpath("//select[@id='StringListId']")));
        se.selectByVisibleText("Public post");

        actions.clickElement("create.post.page.post.message");
        actions.typeValueInField(postContent, "create.post.page.post.message");

        actions.waitForElementClickable("create.post.page.save.post.button");
        actions.clickElement("create.post.page.save.post.button");


    }

    public void likePost(String postContent) {


        navigateToPage();

        actions.waitForElementClickable("//a[contains(text(), 'Latest Posts')]");//Latest posts
        actions.clickElement("//a[contains(text(), 'Latest Posts')]");



        ////*[@id='submit-val165' and @value='Like'] - topic.page.like.post.button]
        ////div[1]/div/div[1]/div/div[2]/form/input[@value='Like']

        if (actions.isElementVisible("//div[1]/div/div[1]/div/div[2]/form/input[@value='Like']")) {
            actions.clickElement("//div[1]/div/div[1]/div/div[2]/form/input[@value='Like']");
        } else {

            ////*[@id='submit-val165' and @value='Dislike'] - topic.page.dislike.post.button
            actions.clickElement("//div[1]/div/div[1]/div/div[2]/form/input[@value='Dislike']");
            actions.clickElement("//div[1]/div/div[1]/div/div[2]/form/input[@value='Dislike']");
        }

        actions.waitForElementVisible("//div[1]/div/div[1]/div/div[2]/form/input[@value='Dislike']");
        actions.assertElementPresent("//div[1]/div/div[1]/div/div[2]/form/input[@value='Dislike']");
    }

    public void dislikePost(String postContent) {


        navigateToPage();

        actions.waitForElementClickable("//a[contains(text(), 'Latest Posts')]");//Latest posts
        actions.clickElement("//a[contains(text(), 'Latest Posts')]");



        ////*[@id='submit-val165' and @value='Like'] - topic.page.like.post.button]

        if (actions.isElementVisible("//div[1]/div/div[1]/div/div[2]/form/input[@value='Like']")) {
            actions.clickElement("//div[1]/div/div[1]/div/div[2]/form/input[@value='Like']");
            actions.clickElement("//div[1]/div/div[1]/div/div[2]/form/input[@value='Dislike']");
        } else {

            ////*[@id='submit-val165' and @value='Dislike'] - topic.page.dislike.post.button
            actions.clickElement("//div[1]/div/div[1]/div/div[2]/form/input[@value='Dislike']");

        }

        actions.waitForElementVisible("//div[1]/div/div[1]/div/div[2]/form/input[@value='Like']");
        actions.assertElementPresent("//div[1]/div/div[1]/div/div[2]/form/input[@value='Like']");
    }

    public void commentPost(String commentContent) {

        navigateToPage();

        actions.waitForElementClickable("//a[contains(text(), 'Latest Posts')]");//Latest posts
        actions.clickElement("//a[contains(text(), 'Latest Posts')]");

        actions.waitForElementVisible("//div[1]/div/div[2]/p[3]/a[1]");//Explore this post
        actions.clickElement("//div[1]/div/div[2]/p[3]/a[1]");

        actions.waitForElementVisible("//textarea");
        actions.typeValueInField(commentContent,"//textarea");

        actions.waitForElementClickable("//input[@value='Post Comment']");
        actions.clickElement("//input[@value='Post Comment']");




    }
}
