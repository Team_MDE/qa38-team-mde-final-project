package pages.telerik.wearesocial;

import org.openqa.selenium.WebDriver;

public class PersonalHomePage extends BaseWebsitePage {

    public PersonalHomePage(WebDriver driver) {
        super(driver, "personal.login.home.page");
    }

    public void navigateToCreatePostPage() {

        navigateToPage();

        actions.waitForElementClickable("personal.home.page.add.new.post");
        actions.clickElement("personal.home.page.add.new.post");

    }

    public void navigateToPersonalProfilePage() {

        navigateToPage();

        actions.waitForElementClickable("personal.profile.button");
        actions.clickElement("personal.profile.button");

    }

    public void navigateToLatestPostsPage() {

        navigateToPage();

        actions.waitForElementClickable("personal.home.page.latest.posts");
        actions.clickElement("personal.home.page.latest.posts");

    }

}