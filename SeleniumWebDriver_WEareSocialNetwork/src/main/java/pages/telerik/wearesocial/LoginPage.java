package pages.telerik.wearesocial;

import org.openqa.selenium.WebDriver;


public class LoginPage extends BaseWebsitePage{

    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void loginUser(String username, String password){

        navigateToPage();

        actions.waitForElementClickable("home.page.login.button");
        actions.clickElement("home.page.login.button");


        actions.typeValueInField(username, "login.page.username");
        actions.typeValueInField(password, "login.page.password");

        actions.waitForElementClickable("login.page.login.button");
        actions.clickElement("login.page.login.button");
    }
}
