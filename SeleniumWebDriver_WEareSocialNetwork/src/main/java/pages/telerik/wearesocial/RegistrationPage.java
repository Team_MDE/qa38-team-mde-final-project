package pages.telerik.wearesocial;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;


public class RegistrationPage extends BaseWebsitePage {
    public RegistrationPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void registerUser(String username, String email, String password, String confirm, String profession) {

        navigateToPage();

        actions.waitForElementClickable("home.page.register.button");
        actions.clickElement("home.page.register.button");

        actions.waitForElementClickable("register.page.form");
        actions.typeValueInField(username, "register.page.username");
        actions.typeValueInField(email, "register.page.email");
        actions.typeValueInField(password, "register.page.password");
        actions.typeValueInField(confirm, "register.page.confirm");

        Select se = new Select(driver.findElement(By.xpath("//*[@id='category.id']")));
        se.selectByVisibleText(profession);

        actions.clickElement("register.page.register.button");

    }

    public void assertUserIsRegistered() {
        actions.waitForElementClickable("register.page.update.profile.button");
        actions.assertElementPresent("register.page.update.profile.button");

    }

    public void assertErrorMessage(String expectedErrorMessage) {
        String actualMessage = driver.findElement(By.xpath("//div[@class='main-login main-center ']/*[2]")).getText();
        Assert.assertEquals(actualMessage, expectedErrorMessage);

    }

}
