package pages.telerik.wearesocial;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;
//
//import java.util.List;

import static com.telerikacademy.testframework.Utils.*;
import static java.lang.String.format;

public class PersonalProfilePage extends BaseWebsitePage {

    public PersonalProfilePage(WebDriver driver, int userId) {
        super(driver, format((getConfigPropertyByKey("personal.profile.page")), userId));
    }

    public void selectPostByPostId(int postId) {

        JavascriptExecutor js = (JavascriptExecutor) actions.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", actions.getDriver().findElement(By.xpath("//span[text()='Recent']")));

        actions.waitForElementVisible("//span[text()='Recent']");
        actions.waitForElementVisible(format((getUIMappingByKey("personal.profile.page.postId")), postId));
        actions.clickElement(format((getUIMappingByKey("personal.profile.page.postId")), postId));

    }

    public void logout() {

        actions.waitForElementVisible(getUIMappingByKey("personal.logout.button"));
        actions.clickElement(getUIMappingByKey("personal.logout.button"));

    }

}
