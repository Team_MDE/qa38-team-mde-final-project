package pages.telerik.wearesocial;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static java.lang.String.format;

public class EditPostPage extends BaseWebsitePage {
    public EditPostPage(WebDriver driver, int postId) {
        super(driver, format((getConfigPropertyByKey("personal.edit.post.page")), postId));
    }

    public void editPost(String postContent, boolean visibility) {

        String locator;
        if (visibility) {
            locator = "create.post.page.visibility.public";
        } else {
            locator = "create.post.page.visibility.private";
        }

        actions.waitForElementClickable("create.post.page.visibility");
        actions.clickElement("create.post.page.visibility");
        actions.clickElement(locator);

        actions.clickElement("create.post.page.post.message");
        actions.typeValueInField(postContent, "create.post.page.post.message");

        actions.waitForElementClickable("create.post.page.save.post.button");
        actions.clickElement("create.post.page.save.post.button");

    }

}
