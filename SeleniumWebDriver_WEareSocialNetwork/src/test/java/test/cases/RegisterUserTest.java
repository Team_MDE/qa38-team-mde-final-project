package test.cases;

import pages.telerik.wearesocial.RegistrationPage;
import org.junit.Test;

import static com.telerikacademy.testframework.Utils.LOGGER;

public class RegisterUserTest extends BaseTestSetup {

    @Test
    public void whenValidCredentials_areEntered_RegistrationIsSuccessfulTest() {

        username = random();
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser(username, "krasi@abv.bg", PASSWORD,
                PASSWORD, "Designer");
        registrationPage.assertUserIsRegistered();


        LOGGER.info("User is successfully registered");

    }

}
