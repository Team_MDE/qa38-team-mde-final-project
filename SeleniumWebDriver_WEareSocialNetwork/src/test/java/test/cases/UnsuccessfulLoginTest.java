package test.cases;

import org.junit.Test;
import pages.telerik.wearesocial.LoginPage;

import static com.telerikacademy.testframework.Utils.LOGGER;

public class UnsuccessfulLoginTest extends BaseTestSetup {

    @Test
    public void whenWrongPassword_isEntered_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser(USERNAME, "wrong");

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
        LOGGER.info("Unsuccessful login with wrong password");
    }

    @Test
    public void whenWrongUsername_isEntered_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("wrong", PASSWORD);

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
        LOGGER.info("Unsuccessful login with wrong username");
    }

    @Test
    public void whenPassword_isEmpty_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser(USERNAME, "");

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
        LOGGER.info("Unsuccessful login with empty password");
    }

    @Test
    public void whenUsername_isEmpty_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("", PASSWORD);

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
        LOGGER.info("Unsuccessful login with empty username");
    }
}
