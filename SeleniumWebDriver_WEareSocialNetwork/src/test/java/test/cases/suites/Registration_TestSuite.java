package test.cases.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.cases.RegisterUserTest;
import test.cases.UnsuccessfulRegistrationWithJUnitTest;

@Suite.SuiteClasses({RegisterUserTest.class, UnsuccessfulRegistrationWithJUnitTest.class})
@RunWith(Suite.class)

public class Registration_TestSuite {
}
