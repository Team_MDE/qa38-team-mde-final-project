package test.cases.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.cases.*;

@Suite.SuiteClasses({RegisterUserTest.class, LoginEnd2End_TestSuiteTest.class, CreatePostTest.class, CreateCommentTest.class, EditCommentTest.class, DeleteCommentTest.class, EditPostTest.class, DeletePostTest.class})
@RunWith(Suite.class)
public class End2End_TestSuite {
}

