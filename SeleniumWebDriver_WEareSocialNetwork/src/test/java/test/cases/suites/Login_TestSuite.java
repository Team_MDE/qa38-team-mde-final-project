package test.cases.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.cases.LoginTest;
import test.cases.UnsuccessfulLoginTest;

@Suite.SuiteClasses({LoginTest.class, UnsuccessfulLoginTest.class})
@RunWith(Suite.class)
public class Login_TestSuite {
}

