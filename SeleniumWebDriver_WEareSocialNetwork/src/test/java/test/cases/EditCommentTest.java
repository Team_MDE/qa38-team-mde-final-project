package test.cases;

import  com.telerikacademy.testframework.data.Constants;
import org.junit.Assert;
import org.junit.Test;
import pages.telerik.wearesocial.ExplorePostPage;
import pages.telerik.wearesocial.LatestPostsPage;
import pages.telerik.wearesocial.PersonalHomePage;

public class EditCommentTest extends BaseTestSetup{

    int count=0;

    String tempUsername;

    @Test
    public void successfulEditCommentToPostTest() {
        tempUsername=username;
        username=usernameComment;
        login();
        username=tempUsername;

        PersonalHomePage personalHomePage = new PersonalHomePage(actions.getDriver());
        personalHomePage.navigateToLatestPostsPage();

        LatestPostsPage latestPostsPage= new LatestPostsPage(actions.getDriver());
        latestPostsPage.clickBrowsePublicPostsButton();
        latestPostsPage.selectPostByPostId(Constants.postId);

        ExplorePostPage explorePostPage = new ExplorePostPage(actions.getDriver(),Constants.postId);
        count=explorePostPage.editComment(Constants.commentId,commentEditMessage);

        Assert.assertEquals("New comment is not created",0,count);

    }

}
