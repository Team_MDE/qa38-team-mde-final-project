package test.cases;

import com.telerikacademy.testframework.data.Constants;
import org.junit.Assert;
import org.junit.Test;
import pages.telerik.wearesocial.*;

public class CreateCommentTest extends BaseTestSetup {

    int count=0;
    String tempUsername;
    int[] arrayIncreaseAndId=new int[2];
    @Test
    public void successfulCreateCommentToPostTest() {

        tempUsername=username;
        registration();
        login();
        usernameComment=username;
        username=tempUsername;
        PersonalHomePage personalHomePage = new PersonalHomePage(actions.getDriver());
        personalHomePage.navigateToLatestPostsPage();

        LatestPostsPage latestPostsPage= new LatestPostsPage(actions.getDriver());
        latestPostsPage.clickBrowsePublicPostsButton();
        latestPostsPage.selectPostByPostId(Constants.postId);

        ExplorePostPage explorePostPage = new ExplorePostPage(actions.getDriver(), Constants.postId);
        arrayIncreaseAndId = explorePostPage.addComment(commentMessage);

        Assert.assertEquals("Comment is not created",1,arrayIncreaseAndId[0]);

        Constants.commentId=arrayIncreaseAndId[1];

    }

}
