package test.cases.magdalena;

import com.telerikacademy.testframework.UserActions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.telerik.wearesocial.LoginPage;

public class BaseTestSetUp {
    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("home.page");
    }

    @After
    public void quitDriver() {
        UserActions.quitDriver();
    }
    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }

    public void login(String username, String password) {

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser(username, password);
    }
}
