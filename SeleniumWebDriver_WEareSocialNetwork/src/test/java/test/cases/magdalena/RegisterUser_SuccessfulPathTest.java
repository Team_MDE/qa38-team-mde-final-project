package test.cases.magdalena;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import pages.telerik.wearesocial.magdalena.RegistrationPage;


import static com.telerikacademy.testframework.Utils.LOGGER;

public class RegisterUser_SuccessfulPathTest extends BaseTestSetUp{
    public String randomUsername() {

        String randomName = "Stela" + RandomStringUtils.randomAlphabetic(3);
        String username = String.valueOf(randomName);
        return username;

    }

    String username = randomUsername();
    @Test
    public void successfulUserRegistrationTest() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser(username, "marina@abv.bg", "marinaMARINA",
                "marinaMARINA", "Designer");
        registrationPage.assertUserIsRegistered();
        LOGGER.info("User successfully registered");

    }
}
