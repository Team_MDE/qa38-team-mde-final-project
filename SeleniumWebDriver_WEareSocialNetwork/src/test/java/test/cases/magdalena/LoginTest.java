package test.cases.magdalena;

import org.junit.Test;
import pages.telerik.wearesocial.magdalena.LoginPage;


public class LoginTest extends BaseTestSetUp{
    @Test
    public void successfulUserLoginTest() {

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("Kamen","kamenKAMEN");

        actions.assertElementPresent("personal.profile.button");
    }

    @Test
    public void whenWrongPassword_isEntered_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("Kamen", "wrong");

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
    }

    @Test
    public void whenWrongUsername_isEntered_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("wrong", "kamenKAMEN");

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
    }

    @Test
    public void whenPassword_isEmpty_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("Kamen", "");

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
    }

    @Test
    public void whenUsername_isEmpty_LoginIsUnsuccessful() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("", "kamenKAMEN");

        actions.assertElementPresent("login.page.wrong.password.or.username.message");
    }
}
