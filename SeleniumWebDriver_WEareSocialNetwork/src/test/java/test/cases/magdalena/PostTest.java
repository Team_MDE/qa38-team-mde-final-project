package test.cases.magdalena;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.telerik.wearesocial.magdalena.PostPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PostTest extends BaseTestSetUp{
    public String randomPost() {

        String randomPost = "My newly created post" + RandomStringUtils.randomNumeric(10);
        String randomComment="My comment" + RandomStringUtils.randomNumeric(3);
        String postContent = String.valueOf(randomPost);
        String commentContent = String.valueOf(randomComment);
        return postContent;

    }

    public String randomComment() {

        String randomComment="My comment" + RandomStringUtils.randomNumeric(3);
        String commentContent = String.valueOf(randomComment);
        return commentContent;

    }

    String postContent = randomPost();
    String commentContent = randomComment();

    @Test

    public void A_createPostTest(){
        login("Kamen", "kamenKAMEN");
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createPost(postContent);
        actions.assertElementPresent("//*[.='" + postContent+ "']");


    }



    @Test
    public void B_likePostTest(){
        login("Kamen", "kamenKAMEN");
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.likePost(postContent);

    }



    @Test
    public void C_dislikePostTest(){
        login("Kamen", "kamenKAMEN");
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.dislikePost(postContent);
    }

    @Test
    public void D_commentPostTest(){
        login("Kamen", "kamenKAMEN");
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createPost(postContent);
        postPage.commentPost(commentContent);
        actions.assertElementPresent("//*[.='" + commentContent+ "']");
    }

}
