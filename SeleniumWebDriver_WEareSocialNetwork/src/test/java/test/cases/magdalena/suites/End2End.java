package test.cases.magdalena.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.cases.magdalena.LoginTest;
import test.cases.magdalena.PostTest;
import test.cases.magdalena.RegisterUser_SuccessfulPathTest;
import test.cases.magdalena.RegisterUser_UnsuccessfulPathTest;

@Suite.SuiteClasses({RegisterUser_SuccessfulPathTest.class, RegisterUser_UnsuccessfulPathTest.class, LoginTest.class, PostTest.class})
@RunWith(Suite.class)
public class End2End {
}
