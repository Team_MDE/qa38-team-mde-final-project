package test.cases.magdalena;

import org.junit.Test;
import pages.telerik.wearesocial.RegistrationPage;

public class RegisterUser_UnsuccessfulPathTest extends BaseTestSetUp{

//When user provides invalid username + valid email, password, confirm password, profession

    @Test
    public void whenUsernameWithDigits_isEntered_RegistrationIsUnsuccessful() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser("11", "zero@abv.bg", "zeroZero", "zeroZero", "Designer");
        registrationPage.assertErrorMessage("username requires no whitespaces, only character");

    }

    @Test
    public void whenUsernameWithWhiteSpaces_isEntered_RegistrationIsUnsuccessful() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser("  ", "zero@abv.bg", "zeroZero", "zeroZero", "Designer");
        registrationPage.assertErrorMessage("username requires no whitespaces, only character");

    }

//When user provides invalid email + valid username, password, confirm password, profession

    @Test
    public void whenEmailDoesNotContainAtSymbol_isEntered_RegistrationIsUnsuccessful() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser("Maria", "testAtgmail.com", "zeroZero", "zeroZero", "Designer");
        registrationPage.assertErrorMessage("this doesn't look like valid email");

    }

    @Test
    public void whenEmailIsRandomString_isEntered_RegistrationIsUnsuccessful() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser("Maria", "testgmailcom", "zeroZero", "zeroZero", "Designer");
        registrationPage.assertErrorMessage("this doesn't look like valid email");

    }

    @Test
    public void whenEmailDoNotContainDot_isEntered_RegistrationIsUnsuccessful() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser("Maria", "test@gmailcom", "zeroZero", "zeroZero", "Designer");
        registrationPage.assertErrorMessage("this doesn't look like valid email");

    }

    //When user provides invalid password(zero) + valid username, email, confirm password(zeroZero), profession

    @Test
    public void whenPasswordAndConfirmationPasswordDoNotMatch_areEntered_RegistrationIsUnsuccessful() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser("Maria", "zero@abv.bg", "zero", "zeroZero", "Designer");
        registrationPage.assertErrorMessage("Your password is not confirmed");

    }

    //When user provides invalid password(zero) and confirmation password (zero)+ valid username, email, profession

    @Test
    public void whenPasswordAndConfirmPasswordWithLessThan6Characters_areEntered_RegistrationIsUnsuccessful() {
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser("Maria", "zero@abv.bg", "zero", "zero", "Designer");
        registrationPage.assertErrorMessage("password must be minimum 6 characters");

    }
}
