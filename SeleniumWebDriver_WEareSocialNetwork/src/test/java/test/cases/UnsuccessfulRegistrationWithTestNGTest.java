package test.cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.telerik.wearesocial.RegistrationPage;

import static com.telerikacademy.testframework.Utils.LOGGER;
import static com.telerikacademy.testframework.Utils.getWebDriver;

public class UnsuccessfulRegistrationWithTestNGTest extends BaseTestSetup {

    //The whole class will be removed. It stays here just for the Final exam
    //to show how I use data-provider with TestNG

    @DataProvider(name = "create")
    public Object[][] values() {

        Object[][] dataset = new Object[][]{

                {"11", "zero@abv.bg", "zeroZero", "zeroZero", "Designer",
                        USERNAME_CONTAINS_DIGITS_ERROR_MESSAGE},
                {"  ", "zero@abv.bg", "zeroZero", "zeroZero", "Designer",
                        USERNAME_CONTAINS_TWO_BLANK_SPACES_ERROR_MESSAGE},
                {"Maria", "testAtgmail.com", "zeroZero", "zeroZero", "Designer",
                        INVALID_EMAIL_ERROR_MESSAGE},
                {"Maria", "testgmailcom", "zeroZero", "zeroZero", "Designer",
                        INVALID_EMAIL_ERROR_MESSAGE},
                {"Maria", "test@gmailcom", "zeroZero", "zeroZero", "Designer",
                        INVALID_EMAIL_ERROR_MESSAGE},
                {"Maria", "zero@abv.bg", "zero", "zeroZero", "Designer",
                        PASSWORD_IS_NOT_CONFIRMED_ERROR_MESSAGE},
                {"Kamen", "kamen@abv.bg", "kamenKAMEN", "kamenKAMEN", "Designer",
                        USERNAME_ALREADY_EXISTS_ERROR_MESSAGE},
                {"Maria", "zero@abv.bg", "zero", "zero", "Designer",
                        PASSWORD_IS_LESS_THAN_6_SYMBOLS_ERROR_MESSAGE}};

        return dataset;
    }

    @Test(dataProvider = "create")
    public void unsuccessfulUserRegistrationTest(String username, String email, String password,
                                                 String confirmPassword, String profession,
                                                 String expectedErrorMessage) {

        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser(username, email, password, confirmPassword, profession);

        LOGGER.info("User provides invalid credentials: '" + username + ", " + password + ", "
                + confirmPassword + ", " + profession + "'");

        registrationPage.assertErrorMessage(expectedErrorMessage);

        LOGGER.info("Error message appears: " + expectedErrorMessage);

    }

    @AfterTest
    public void afterTest() {
        getWebDriver().quit();
    }


}
