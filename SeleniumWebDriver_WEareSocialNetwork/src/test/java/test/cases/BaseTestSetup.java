package test.cases;


import com.telerikacademy.testframework.UserActions;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.telerik.wearesocial.LoginPage;
import pages.telerik.wearesocial.RegistrationPage;


public class BaseTestSetup {

    static String username;
    static String usernameComment;

    public static final String PASSWORD = "krasiKRASI";
    public static final String USERNAME = "StelaUqm";
    public static final String USERNAME_CONTAINS_DIGITS_ERROR_MESSAGE = "username requires no whitespaces, " +
                                                     "only character";
    public static final String USERNAME_CONTAINS_TWO_BLANK_SPACES_ERROR_MESSAGE = "username requires no whitespaces, " +
                                                               "only character";
    public static final String USERNAME_ALREADY_EXISTS_ERROR_MESSAGE = "User with this username already exist";
    public static final String INVALID_EMAIL_ERROR_MESSAGE = "this doesn't look like valid email";
    public static final String PASSWORD_IS_NOT_CONFIRMED_ERROR_MESSAGE = "Your password is not confirmed";
    public static final String PASSWORD_IS_LESS_THAN_6_SYMBOLS_ERROR_MESSAGE =
                                     "password must be minimum 6 characters";

    UserActions actions = new UserActions();
    String postCreateMessage = "Opening New School Year!";
    String postEditMessage = "Happy New School Year!";
    String commentMessage = "Welcome, Kids!";

    String commentEditMessage = "Good Luck, Kids!";
    boolean postCreateVisibility = true;
    boolean postEditVisibility = true;


    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("home.page");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }

    public void login() {

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser(username, PASSWORD);

    }

    public void registration() {

        username = random();
        RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());
        registrationPage.registerUser(username, "krasi@abv.bg", PASSWORD,
                PASSWORD, "Designer");
        registrationPage.assertUserIsRegistered();

    }

    public String random() {

        String randomAlpha = "Stela" + RandomStringUtils.randomAlphabetic(3);
        String user = String.valueOf(randomAlpha);
        return user;

    }

}