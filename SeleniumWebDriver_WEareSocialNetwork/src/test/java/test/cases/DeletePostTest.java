package test.cases;

import com.telerikacademy.testframework.data.Constants;
import org.junit.Test;
import pages.telerik.wearesocial.*;

public class DeletePostTest extends BaseTestSetup {

    @Test
    public void successfulDeletePostTest() {

        login();

        PersonalHomePage personalHomePage = new PersonalHomePage(actions.getDriver());
        personalHomePage.navigateToPersonalProfilePage();

        PersonalProfilePage personalProfilePage = new PersonalProfilePage(actions.getDriver(),41);
        personalProfilePage.selectPostByPostId(Constants.postId);

        ExplorePostPage explorePostPage = new ExplorePostPage(actions.getDriver(), Constants.postId);
        explorePostPage.clickDeletePost(Constants.postId);

        DeletePostPage deletePostPage = new DeletePostPage(actions.getDriver(),Constants.postId);
        deletePostPage.deletePost(true);

        actions.assertElementPresent("delete.post.page.delete.confirmed");

    }

}
